# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from shop.views import * 
from ez_admin.views import *
from django.contrib.auth import views as auth_views
admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^select2/', include('django_select2.urls')),
    
]

urlpatterns += i18n_patterns('',
    url(r'^home/product/(?P<slug>[-\w\d]+)/$', single_product),
    url(r'^home/catalog/$', shop_catalog),
    url(r'^ez_admin/$', ez_admin),
    url(r'^ez_admin/products/$', admin_product),
    url(r'^ez_admin/featured_products/$',featured_product),
    url(r'^add_new_featured_product/$',add_new_featured_product),
    url(r'^delete_featured_product/$',delete_featured_product),
    url(r'^ez_admin/single_product/$', single_admin_product),
    url(r'^ez_admin/single_category/$', single_admin_category),
    url(r'^add/attribute/$',add_attribute),
    url(r'^add/product_attr/$', add_productattribute),
    url(r'^publish/all_products/$', publish_all_product),
    url(r'^publish/selected_products/$',publish_selected_product),
    url(r'^administrator/product/offer/$',offered_product),
    url(r'^update/all/product/last_update/$',update_last_update),
    url(r'^remove/attribute/$',remove_attribute),
    url(r'^delete/pattribute/$', remove_pattribute),
    url(r'^ez_admin/category',category_admin),
    url(r'^home/$',home_shop),
    url(r'^sc_1/$',load_sc_1),
    url(r'^sc_2/$',load_sc_2),
    url(r'^sc_3/$',load_sc_3),
    url(r'^sc_4/$',load_sc_4),
    url(r'^action/scrape/$', scrape),
    url(r'^action/scrape_nationwide/$', scrape_nationwide),
    url(r'^ez_admin/password',reset_password),
    url(r'^ez_admin/profile',admin_profile),
    url(r'^ez_admin/users',users_admin),
    url(r'^admin/settings/configuration', settings_config),
    url(r'^admin/settings/export',output_config_file),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^ez_admin/register/$', register_user),
    url(r'^logout/$', auth_views.logout, {'next_page': '/home'}, name='logout'),
    url(r'^ez_admin/manufacturer_product/$',manufacturer_product),
    url(r'^ez_admin/new_manufacturer_product/$',new_manufacturer),
    url(r'^category_admin_table/$',load_category_table),
    url(r'^product_admin_table/$',load_product_table),
    url(r'^administrator/product/modified/$',add_modified_products),
    url(r'^home/checkout/$',checkout_product),
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^', include('cms.urls')),
    
    
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
