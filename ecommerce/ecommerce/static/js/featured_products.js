function add_new_featured_product()
{
    id = $('#featured_product_new').val();
    $.ajax({
            type: "GET",
            url: "/add_new_featured_product/",
            data: {
                'id':id
            },
            success: function(data){
                console.log(data);
            },
            error: function(){
                console.log("error");
            }
        })
}

function delete_featured_product(id)
{
    if (confirm('Are you sure you want to remove this product from featured section?')) {
        $.ajax({
            type: "GET",
            url: "/delete_featured_product/",
            data: {
                'id':id
            },
            success: function(data){
                console.log(data);
            },
            error: function(){
                console.log("error");
            }
        }).done(function(){
            location.reload();
        });
    } else {
        return;
    }
    
}