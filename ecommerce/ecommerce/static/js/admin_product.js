var price = 0;
function check_all(element)
{
    if(element.checked)
    {
        $('.checkbox').prop('checked', true);
    }
    else
    {
        $('.checkbox').prop('checked', false);
    }
}

function load_products()
{
    var id = 0;
    if($("#sc_1").val()==null || $("#sc_1").val()== 0){
        id = $("#main_c").val();
    }
    else
    {
        if($("#sc_2").val()==null || $("#sc_2").val()== 0){
            id = $("#sc_1").val();
        }
        else
        {
            if($("#sc_3").val()==null || $("#sc_3").val()== 0){
                id = $("#sc_2").val();
            }
            else
            {
                if($("#sc_4").val()==null || $("#sc_4").val()== 0){
                    id = $("#sc_3").val();
                }
                else
                {
                    id = $("#sc_4").val();
                }

            }
        }
    }


    $.ajax({
                type: "GET",
                url: "/product_admin_table/",
                data: {
                    'id': id
                },
                success: function(data){
                    $(".product-content").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })

}

$(document).ready(function(){
    $("#id_factor").on("keyup", function(){
        $("#id_price").val( (price * $(this).val()).toFixed(2));
    });

    $("#btn_all").on('click',function(){
        all();
    });

    $("#btn_selected").on('click',function(){
        selected();
    });
});

$(window).load(function(){
    price = $("#id_price").val();
    $(".sku-lbl").html($("#id_sku").val());

    $("#id_sku").on("keyup", function(){
        $(".sku-lbl").html($("#id_sku").val());
    });


})


function warehouse(id)
{
    url = $("#id_mfr_url").val();
    if(url==""){
        alert("Manufacturer/Supplier Product URL is required");
        return;
    }

    $.LoadingOverlay("show");
    $.ajax({
        type: "GET",
        url: "/action/scrape/",
        data: {
            'url': url,
            'id':id
        },
        success: function(data){
            console.log(data);
            $("#id_mfr_purchase").val(data.price.replace("$", ""));
            $("#id_length").val(data.length.replace(" IN", ""));
            $("#id_weight").val(data.weight.replace(" LB", ""));
            $("#id_stock").val(data.stock);
            $("#id_width").val(data.width.replace(" IN", ""));
            $("#last_update_lbl").html('Last Update: ' + data.lastupdate);
            $(".scrape").addClass("scrape_data");
            $.LoadingOverlay("hide");
        },
        error: function(){
            console.log("error");
            $.LoadingOverlay("hide");
        }
    })
}

function save_attribute()
{
    name = $("#attr_name").val();
    description = $("#attr_description").val();
    $.ajax({
        type: "GET",
        url: "/add/attribute/",
        data: {
            name,
            description
        },
        success: function(data){
            $("#attribute_list").prepend(data);
            $("#attr_name").val("");
            $("#attr_description").val("");
        },
        error: function(){
            console.log("error");
        }
    })
}

function save_product_attribute(id)
{
    name = $("#attr_name_product").val()
    attribute = $("#attr_product").val();
    description = $("#description_prattr").val();
    alteration = $("#alteration_prattr").val();
    value = $("#value_prattr").val();
    if(name==""){
        alert("Name is Required");
        return;
    }

    if(attribute==""){
        alert("Attribute is Required");
        return;
    }

    if(value==""){
        alert("Value is Required");
        return;
    }

    if(alteration==""){
        alert("Alteration is Required");
        return;
    }

    if(description==""){
        alert("Description is Required");
        return;
    }

    

    

    $.ajax({
        type: "GET",
        url: "/add/product_attr/",
        data: {
            attribute,
            value,
            alteration,
            description,
            id,
            name
        },
        success: function(data){
            $("#attribute_product_list").prepend(data);
            $("#value_prattr").val("");
            $("#attribute_prattr").val("");
            $("#description_prattr").val("");
        },
        error: function(){
            console.log("error");
        }
    })
}

function delete_attribute(id)
{
    if (confirm("Are you sure you want to remove the attribute?, if you removed all the products with this attribute will be updated!") == true) {
        $.ajax({
            type: "GET",
            url: "/remove/attribute/",
            data: {
                id,
            },
            success: function(data){
                $("#" + id).remove();
            },
            error: function(){
                console.log("error");
            }
        })
    } 

}

function delete_pattribute(id)
{
    if (confirm("Are you sure you want to remove this product attribute?") == true) {
        $.ajax({
            type: "GET",
            url: "/delete/pattribute/",
            data: {
                id,
            },
            success: function(data){
                $("#p" + id).remove();
            },
            error: function(){
                console.log("error");
            }
        })
    } 

}

function edit_pattribute(value,alteration,description,attribute,name)
{
    $("#value_prattr").val(value);
    $("#alteration_prattr").val(alteration);
    $("#description_prattr").val(description);
    $("#attr_product").val(attribute);
    $("#attr_name_product").val(name)
}

function publish_selected_product()
{
    var checkedValues = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();

    if(checkedValues[0]=="on"){
        $.LoadingOverlay("show");
        $.ajax({
            type: "GET",
            url: "/publish/all_products/",
            success: function(data){
                $(".product-content").html("");
            },
            error: function(){
                $.LoadingOverlay("hide");
                
                console.log("error");
            }
        }).done(function(){
            $.LoadingOverlay("hide");
             location.reload();
        });
    }
    else
    {
        alert("no todos");
    }
}

function all()
{
    action = $("#action_dd").val();
    if (action == 1)
    {

    }
    if (action == 2)
    {
        publish_all();
    }
}

function selected()
{
    action = $("#action_dd").val();
    if (action == 1)
    {

    }
    if (action == 2)
    {
        publish_selected();
    }
}

function publish_all()
{
    $.LoadingOverlay("show");
    $.ajax({
            type: "GET",
            url: "/publish/all_products/",
            success: function(data){
                $(".product-content").html("");
            },
            error: function(){
                $.LoadingOverlay("hide");
                
                console.log("error");
            }
        }).done(function(){
            $.LoadingOverlay("hide");
             location.reload();
        });
}

function publish_selected()
{
    var checkedValues = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();

    $.LoadingOverlay("show");
    for(i=0;i < checkedValues.length; i++)
    {
        $.ajax({
                type: "GET",
                url: "/publish/selected_products/",
                data: {"id": checkedValues[i] },
                success: function(data){
                    $(".product-content").html("");
                },
                error: function(){
                    $.LoadingOverlay("hide");
                    
                    console.log("error");
                }
            }).done(function(){
                 location.reload();
            });
    }
    $.LoadingOverlay("hide");

    
}

function all_product()
{
    $.ajax({
        type: "GET",
        url: "/update/all/product/last_update/",
        success: function(data){
            $(".product-content").html("");
        },
        error: function(){
            $.LoadingOverlay("hide");
            
            console.log("error");
        }
    }).done(function(){
        $.LoadingOverlay("hide");
            location.reload();
    });
}