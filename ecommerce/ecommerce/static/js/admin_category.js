$(document).ready(function(){

    $("#main_c").on('change', function(){
        if ($(this).val()!= 0)
        {
            $.ajax({
                type: "GET",
                url: "/sc_1/",
                data: {
                    'id': $(this).val()
                },
                success: function(data){
                    console.log(data);
                    $("#sc_1").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })
        }
    });



    $("#sc_1").on('change', function(){
        if ($(this).val()!= 0)
        {
            $.ajax({
                type: "GET",
                url: "/sc_2/",
                data: {
                    'id': $(this).val()
                },
                success: function(data){
                    $("#sc_2").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })
        }
    });

    $("#sc_2").on('change', function(){
        if ($(this).val()!= 0)
        {
            $.ajax({
                type: "GET",
                url: "/sc_3/",
                data: {
                    'id': $(this).val()
                },
                success: function(data){
                    $("#sc_3").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })
        }
    });

    $("#sc_3").on('change', function(){
        if ($(this).val()!= 0)
        {
            $.ajax({
                type: "GET",
                url: "/sc_4/",
                data: {
                    'id': $(this).val()
                },
                success: function(data){
                    $("#sc_4").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })
        }
    });

});

function load_category()
{
    var id = 0;
    if($("#sc_1").val()==null || $("#sc_1").val()== 0){
        id = $("#main_c").val();
    }
    else
    {
        if($("#sc_2").val()==null || $("#sc_2").val()== 0){
            id = $("#sc_1").val();
        }
        else
        {
            if($("#sc_3").val()==null || $("#sc_3").val()== 0){
                id = $("#sc_2").val();
            }
            else
            {
                if($("#sc_4").val()==null || $("#sc_4").val()== 0){
                    id = $("#sc_3").val();
                }
                else
                {
                    id = $("#sc_4").val();
                }

            }
        }
    }


    $.ajax({
                type: "GET",
                url: "/category_admin_table/",
                data: {
                    'id': id
                },
                success: function(data){
                    $(".category-content").html(data);
                },
                error: function(){
                    console.log("error");
                }
            })

}


