# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0007_auto_20170302_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='details',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
