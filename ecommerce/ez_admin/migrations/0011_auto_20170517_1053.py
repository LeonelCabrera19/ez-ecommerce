# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0010_configschema'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configschema',
            name='cat_no',
            field=models.CharField(max_length=700, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configschema',
            name='depth',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configschema',
            name='description',
            field=models.CharField(max_length=700, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configschema',
            name='length',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configschema',
            name='weight',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='configschema',
            name='width',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
