# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0003_auto_20170222_1626'),
    ]

    operations = [
        migrations.RenameField(
            model_name='manufacturer',
            old_name='description_short',
            new_name='description',
        ),
    ]
