# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0011_auto_20170517_1053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configschema',
            name='description',
            field=models.TextField(max_length=700, null=True, blank=True),
        ),
    ]
