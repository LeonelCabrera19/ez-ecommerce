# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0008_details_archived'),
    ]

    operations = [
        migrations.AlterField(
            model_name='details',
            name='address',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='address2',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='city',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='company_logo',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='country',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='department',
            field=models.CharField(default=b'', max_length=44, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='organization',
            field=models.CharField(default=b'', max_length=44, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='phone',
            field=models.CharField(default=b'', max_length=14, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='state',
            field=models.CharField(default=b'', max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='title',
            field=models.CharField(default=b'', max_length=44, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='user_image',
            field=models.CharField(default=b'', max_length=800, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='work_phone',
            field=models.CharField(default=b'', max_length=14, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='details',
            name='zipcode',
            field=models.CharField(default=b'', max_length=5, null=True, blank=True),
        ),
    ]
