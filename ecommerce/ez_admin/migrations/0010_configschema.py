# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0009_auto_20170303_1049'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConfigSchema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('qty', models.IntegerField()),
                ('cat_no', models.CharField(max_length=700)),
                ('description', models.CharField(max_length=700)),
                ('weight', models.CharField(max_length=20)),
                ('length', models.CharField(max_length=20)),
                ('width', models.CharField(max_length=20)),
                ('depth', models.CharField(max_length=20)),
                ('factor', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('price', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('purchase', models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True)),
                ('cat_sheet', models.CharField(max_length=20, null=True, blank=True)),
                ('group', models.CharField(max_length=20, null=True, blank=True)),
                ('category', models.CharField(max_length=20, null=True, blank=True)),
                ('function', models.CharField(max_length=20, null=True, blank=True)),
                ('addressable', models.CharField(max_length=20, null=True, blank=True)),
                ('fixture', models.CharField(max_length=20, null=True, blank=True)),
                ('construction', models.CharField(max_length=20, null=True, blank=True)),
                ('subcategory', models.CharField(max_length=20, null=True, blank=True)),
                ('title', models.CharField(max_length=20, null=True, blank=True)),
            ],
        ),
    ]
