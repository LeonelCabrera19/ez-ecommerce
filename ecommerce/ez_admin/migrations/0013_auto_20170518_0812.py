# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0012_auto_20170518_0811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configschema',
            name='description',
            field=models.TextField(null=True, blank=True),
        ),
    ]
