# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0002_auto_20170222_1621'),
    ]

    operations = [
        migrations.RenameField(
            model_name='manufacturer',
            old_name='description',
            new_name='description_short',
        ),
        migrations.AlterField(
            model_name='manufacturer',
            name='title',
            field=models.CharField(max_length=800),
        ),
        migrations.AlterField(
            model_name='manufacturer',
            name='url',
            field=models.CharField(max_length=800, null=True, blank=True),
        ),
    ]
