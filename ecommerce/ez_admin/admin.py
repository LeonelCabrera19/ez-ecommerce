from django.contrib import admin
from ez_admin.models import *
# Register your models here.

class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Manufacturer, ManufacturerAdmin)
