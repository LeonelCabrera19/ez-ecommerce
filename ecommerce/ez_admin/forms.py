import re
from django import forms
from .models import *

class ManufacturerForm(forms.ModelForm):
    class Meta:
        model = Manufacturer
        exclude = ()
