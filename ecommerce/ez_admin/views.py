from django.shortcuts import render
from slugify import slugify
from django.contrib.auth import update_session_auth_hash
from shop.models import *
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from shop.forms import *
from ez_admin.forms import *
from ez_admin.models import *
from ez_admin.templatetags import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
import requests
from lxml import html
import mechanize
import cookielib
from BeautifulSoup import BeautifulSoup
import html2text
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
import json
import openpyxl
import xlsxwriter
from django.core import serializers
# Create your views here.
# Create your views here.
def ez_admin(request):
    product = Product.objects.all()
    user = User.objects.filter(is_active=True)
    category = Category.objects.all()
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'title': 'test',
        'user': user,
        'product': product,
        'category': category,
        'product_edited':product_edited.count
    }
    return render(request, 'welcome.html',context)

def admin_product(request):
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    products = Product.objects.all()
    categories = Category.objects.all()
    context = {
        'title': 'test',
        'products':products,
        'categories':categories,
        'product_edited':product_edited.count
    }
    return render(request, 'admin_products.html',context)

def admin_profile(request):
    context = {
        'title': 'test',
    }
    return render(request, 'profile.html',context)

def shop_catalog(request):
    category = request.GET.get("category")
    page = request.GET.get('page')
    if category != None:
        currente_category = Category.objects.get(id=category)
        products_list = Product.objects.filter(state=1).filter(category=currente_category)
    else:
        products_list = Product.objects.filter(state=1)

    paginator = Paginator(products_list, 9)

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    category = Category.objects.all()
    context = {
        'products': products,
        'categories': category,
        'products_list':products_list
    }
    return render(request, 'shop_catalog.html',context)

def reset_password(request):
    error = ""
    if request.method == 'POST':
        if not request.user.is_authenticated():
            return render(request, 'restore_password.html')

        form = PasswordChangeForm(request.user, request.POST)
        error = form.errors
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return HttpResponseRedirect('/login/')
    else:
        form = PasswordChangeForm(request.user)

    return render(request, 'restore_password.html', {
        'form': form,
        'errors': error
    })

def register_user(request):
    id = request.GET.get("id")
    update = request.GET.get("edit")
    remove = request.GET.get("remove")
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    if remove=="1":
        
        try:
            user_remove = User.objects.get(id=id)
            user_remove.is_active = False
            user_remove.save()
        except User.DoesNotExist:
            print("Error")

        return HttpResponseRedirect('/ez_admin/users')
        
    if update=="1":
        if request.method == 'POST':
            edit_user = User.objects.get(id=id)
            
            username = request.POST.get("username")
            email = request.POST.get("email")
            phone = request.POST.get("phone")
            work_phone = request.POST.get("work-phone")
            address = request.POST.get("address")
            address2 = request.POST.get("address2")
            country = request.POST.get("country")
            state = request.POST.get("state")
            city = request.POST.get("city")
            zipcode = request.POST.get("zipcode")
            first_name = request.POST.get("first_name")
            last_name = request.POST.get("last_name")
            title = request.POST.get("title")
            department = request.POST.get("department")

            edit_user.username = username
            edit_user.first_name = first_name
            edit_user.last_name = last_name
            edit_user.email = email
            edit_user.save()

            try:
                edit_detail = Details.objects.get(user=edit_user)
            except Details.DoesNotExist:
                edit_detail = Details()
                edit_detail.user = edit_user

            edit_detail.phone = phone
            edit_detail.work_phone = work_phone
            edit_detail.address = address
            edit_detail.address2 = address2
            edit_detail.country = country
            edit_detail.state = state
            edit_detail.city = city
            edit_detail.zipcode = zipcode
            edit_detail.title = title
            edit_detail.department = department
            edit_detail.save()
            
            return HttpResponseRedirect('/ez_admin/users')
        else:
            edit_user = User.objects.get(id=id)
            context = {
                    'title': 'test',
                    'edit_user': edit_user,
                    'product_edited':product_edited.count
                }
    else:
        if request.method == 'POST':
            username = request.POST.get("username")
            email = request.POST.get("email")
            password = request.POST.get("password")
            first_name = request.POST.get("first_name")
            last_name = request.POST.get("last_name")
            phone = request.POST.get("phone")
            work_phone = request.POST.get("work-phone")
            address = request.POST.get("address")
            address2 = request.POST.get("address2")
            country = request.POST.get("country")
            state = request.POST.get("state")
            city = request.POST.get("city")
            zipcode = request.POST.get("zipcode")
            title = request.POST.get("title")
            department = request.POST.get("department")

            new_user = User.objects.create_user(username, email, password)
            new_user.first_name = first_name
            new_user.last_name = last_name
            new_user.save()

            detail = Details()
            detail.user = new_user
            detail.phone = phone
            detail.work_phone = work_phone
            detail.address = address
            detail.address2 = address2
            detail.country = country
            detail.state = state
            detail.city = city
            detail.zipcode = zipcode
            detail.title = title
            detail.department = department
            detail.save()
            

            return HttpResponseRedirect('/ez_admin/users')
        else:
            context = {
                'title': 'test',
                'edit':False,
                'product_edited':product_edited.count
            }

    return render(request, 'register_user.html',context)

def users_admin(request):
    user = User.objects.all()
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'title': 'title',
        'edit': False,
        'users': user,
        'product_edited':product_edited.count
    }
    return render(request, 'user_admin.html', context)

def featured_product(request):
    products = Product.objects.filter(home=True)
    all_products = Product.objects.filter(home=False)
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'title': 'test',
        'products':products,
        'all_products':all_products,
        'product_edited':product_edited.count
    }
    return render(request, 'featured_products.html',context)

def offered_product(request):
    products = Product.objects.filter(home=True)
    all_products = Product.objects.filter(home=False)
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'title': 'test',
        'products':products,
        'all_products':all_products,
        'product_edited':product_edited.count
    }
    return render(request, 'offer_products.html',context)

def manufacturer_product(request):
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    manufacturers = Manufacturer.objects.all()
    context = {
        'title': 'test',
        'manufacturers':manufacturers,
        'product_edited':product_edited.count
    }
    return render(request, 'manufacturer_admin.html',context)

def settings_config(request):
    wb = openpyxl.load_workbook('EZFire_Input_File_2_bk.xlsx')
    sheet = wb.get_sheet_by_name('Sheet1')

    payload = {
        "PIN": "256661", 
        "Password": "Rafael120588168"
    }
    session_requests = requests.session()
    login_url = "https://myeddie.edwardsutcfs.com/Logon/Logon"
    result = session_requests.post(
        login_url, 
        data = payload, 
        headers = dict(referer=login_url)
    )
    products_config = ConfigSchema.objects.all()
    products_config.delete()
    last_product = ""
    for i in range(2, 1273):
        product = ConfigSchema()
        letter = "B"
        letter = letter + "" + str(i)
        
        url = "https://myeddie.edwardsutcfs.com/Products/ProductSearch?SearchTerm=" + str(sheet[letter].value)
        result = session_requests.get(
            url, 
            headers = dict(referer = url)
        )
        tree = html.fromstring(result.content)
        bucket_products = tree.findall(".//div[@class='pd-result']")
        
        
        if last_product != str(sheet[letter].value):

            for div in bucket_products:
                
                bucket_elems = div.findall(".//div[@class='column-entry']/ul/li")
                bucket_elems_2 = tree.findall(".//li[@class='featureValue']")
                bucket_validation = div.findall(".//h2[@id='no-results']")
                bucket_description = div.findall(".//div[@class='pd-description']/p")
                bucket_title = div.findall(".//div[@class='product-number']/h2")
                bucket_clearence = div.findall(".//li[@class='clearance_price']")
                print(str(sheet[letter].value))
                for bucket in bucket_elems:
                    print(bucket.text.strip())

                if(len(bucket_elems) != 0):
                    product.cat_no = str(sheet[letter].value)
                    if len(bucket_elems_2) > 3:
                        product.weight = bucket_elems_2[0].text.strip()
                        product.length = bucket_elems_2[1].text.strip()
                        product.width = bucket_elems_2[2].text.strip()
                        product.depth = bucket_elems_2[3].text.strip()
                    if len(bucket_clearence) == 0:
                        product.purchase = bucket_elems[1].text.strip().replace("$","").replace(",","")
                        product.price = bucket_elems[0].text.strip().replace("$","").replace(",","")
                        product.qty = bucket_elems[2].text.strip().replace("$","").replace(",","").replace(".","")
                    else:
                        product.purchase = bucket_elems[2].text.strip().replace("$","").replace(",","")
                        product.price = bucket_elems[1].text.strip().replace("$","").replace(",","")
                        product.qty = bucket_elems[3].text.strip().replace("$","").replace(",","").replace(".","")

                product.factor = 2
                product.description = bucket_description[0].text.strip()
                serialized_obj = serializers.serialize('json', [ product, ])
                print(serialized_obj)
                product.save()
                last_product = str(sheet[letter].value)
                #WRITE NEW EXCEL
                #1273
                #B-T
        else:
            print("repeated")
    
    return render(request, 'config_admin.html')


def output_config_file(request):
    workbook = xlsxwriter.Workbook(slugify(datetime.datetime.today()) + '.xlsx')
    worksheet = workbook.add_worksheet()

    # Replace this with your dataset
    products = ConfigSchema.objects.all()

    bold = workbook.add_format({'bold': True})

    worksheet.write('A1', 'Qty', bold)
    worksheet.write('B1', 'Cat No', bold)
    worksheet.write('C1', 'Description', bold)
    worksheet.write('D1', 'Weight (lbs)', bold)
    worksheet.write('E1', 'Length (in.)', bold)
    worksheet.write('F1', 'Width (in.)', bold)
    worksheet.write('G1', 'Depth (in.)', bold)
    worksheet.write('H1', 'Factor', bold)
    worksheet.write('I1', 'Sale Price (MSRP Trade Price)', bold)
    worksheet.write('J1', 'Purchase Price', bold)
    worksheet.write('K1', 'Cat. Sheet #', bold)
    worksheet.write('L1', 'Group', bold)
    worksheet.write('M1', 'CATEGORY', bold)
    worksheet.write('N1', 'FUNCTION', bold)
    worksheet.write('O1', 'ADDRESSABLE', bold)
    worksheet.write('P1', 'FIXTURE', bold)
    worksheet.write('Q1', 'CONSTRUCTION', bold)
    worksheet.write('R1', 'SubCategory5', bold)
    worksheet.write('S1', 'Title', bold)
    last_cat_no = ""
    for index, item in enumerate(products):
        index += 1
        if last_cat_no != item.cat_no:
            worksheet.write(index, 0, item.qty)
            worksheet.write(index, 1, item.cat_no)
            worksheet.write(index, 2, item.description)
            worksheet.write(index, 3, item.weight)
            worksheet.write(index, 4, item.length)
            worksheet.write(index, 5, item.width)
            worksheet.write(index, 6, item.depth)
            worksheet.write(index, 7, item.factor)
            worksheet.write(index, 8, item.price)
            worksheet.write(index, 9, item.price)
            worksheet.write(index, 10, item.cat_sheet)
            worksheet.write(index, 11, item.group)
            worksheet.write(index, 12, item.category)
            worksheet.write(index, 13, item.function)
            worksheet.write(index, 14, item.addressable)
            worksheet.write(index, 15, item.fixture)
            worksheet.write(index, 16, item.construction)
            worksheet.write(index, 17, item.subcategory)
            worksheet.write(index, 18, item.title)
            last_cat_no = item.cat_no
        else:
            print("repetido")

    workbook.close()
    return render(request, 'config_admin.html')

def new_manufacturer(request):
    id = request.GET.get("id")
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    if(id!=None):
        manufacturer = Manufacturer.objects.get(id=id)
        delete = request.GET.get("delete")
        if(delete!=None):
            manufacturer.delete()
            return HttpResponseRedirect("/ez_admin/manufacturer_product/")
            
        if request.method == "POST":
            form = ManufacturerForm(request.POST,instance = manufacturer)
            print(form.errors)
            form.save()
            return HttpResponseRedirect("/ez_admin/manufacturer_product/")
        else:
            manufacturer_form = ManufacturerForm(instance=manufacturer)
            context = {
                'create': False,
                'manufacturer_form':manufacturer_form,
                'product_edited':product_edited.count
            }
            return render(request, 'new_manufacturer.html',context)
    else:
        if request.method == "POST":
            form = ManufacturerForm(request.POST)
            print(form.errors)
            form.save()
            return HttpResponseRedirect("/ez_admin/manufacturer_product/")
        else:
            manufacturer_form = ManufacturerForm()
            context = {
                'create': True,
                'manufacturer_form':manufacturer_form,
                'product_edited':product_edited.count
            }
            return render(request, 'new_manufacturer.html',context)


def single_admin_product(request):
    id = request.GET.get("id")
    edit = request.GET.get("edit")
    list_attributes = Attribute.objects.all()
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    if edit == "0":
        if request.method == "POST":
            form = ProductForm(request.POST)
            print(form.errors)
            last_input = form.save(commit=False)
            last_input.state = 2
            last_input.save()
            return HttpResponseRedirect("/ez_admin/single_product/?id="+ str(last_input.pk))
        else:
            product_form = ProductForm()
            context = {
                'product_form':product_form,
                'edit': False,
                'list_attributes':list_attributes,
                'id':id,
                'product_edited':product_edited.count
            }
            return render(request, 'single_admin_product.html',context)
    else:
        product = Product.objects.get(id=id)
        related_products = ProductRelated.objects.filter(product_father_id=product)
        list_product_attributes = AttributeProduct.objects.filter(product=product)
        if request.method == "POST":
            form = ProductForm(request.POST, instance=product)
            last_input = form.save(commit=False)
            last_input.state = 3
            last_input.save()
            return HttpResponseRedirect("/ez_admin/single_product/?id="+id)
        else:
            product_form = ProductForm(instance=product)
            context = {
                'title': 'test',
                'product': product,
                'last_update': product.last_update,
                'slug': product.slug,
                'product_form':product_form,
                'related_products': related_products,
                'list_attributes': list_attributes,
                'list_product_attributes':list_product_attributes,
                'id':id,
                'edit': True,
                'product_edited':product_edited.count
            }
            return render(request, 'single_admin_product.html',context)


def category_admin(request):
    categories = Category.objects.all()
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'title': 'test',
        'categories':categories,
        'product_edited':product_edited.count
    }
    return render(request, 'category_admin.html', context)

def single_admin_category(request):
    id = request.GET.get("id")
    edit = request.GET.get("edit")
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    
    if(id!=None):
        category = Category.objects.get(id=id)
        delete = request.GET.get("delete")
        if(delete!=None):
            category.delete()
            return HttpResponseRedirect("/ez_admin/category/")
            
        if request.method == "POST":
            form = CategoryForm(request.POST,instance = category)
            print(form.errors)
            form.save()
            return HttpResponseRedirect("/ez_admin/category/")
        else:
            category_form = CategoryForm(instance=category)
            context = {
                'create': False,
                'category_form':category_form,
                'product_edited':product_edited.count
            }
            return render(request, 'single_admin_category.html',context)
    else:
        if request.method == "POST":
            form = CategoryForm(request.POST)
            print(form.errors)
            form.save()
            return HttpResponseRedirect("/ez_admin/category/")
        else:
            category_form = CategoryForm()
            print(product_edited.count)
            context = {
                'create': True,
                'category_form':category_form,
                'product_edited':product_edited.count
            }
            return render(request, 'single_admin_category.html',context)

@csrf_exempt
def add_modified_products(request):
    product_edited = Product.objects.filter(Q(state=2) | Q(state=3))
    context = {
        'product_edited':product_edited.count,
        'products':product_edited
    }
    return render(request, 'add_modified_product.html',context)

@csrf_exempt
def add_new_featured_product(request):
    product_id = request.GET.get("id")
    print(product_id)
    product = Product.objects.get(id=product_id)
    product.home = True
    product.save()
    return HttpResponse("1")

@csrf_exempt
def load_category_table(request):
    category_id = request.GET.get("id")
    print(category_id)
    categories = Category.objects.filter(father_category=category_id)
    table = ""
    for sc_1 in categories:
        table = table +  "<tr>"
        table = table + "<td>" + str(sc_1.id) +"</td>"
        table = table + "    <td>" + str(sc_1.title) + "</td>"
        table = table + "    <td>" + str(sc_1.father_category) +"</td>"
        table = table + "    <td>"
        table = table + "        <a href='/ez_admin/single_product/?id="+ str(sc_1.id) + "'>"
        table = table + "        <button type='button' class='btn btn-block btn-primary btn-flat'>Edit</button>"
        table = table + "        </a>"
        table = table + "    </td>"
        table = table + "</tr>"
    print(table)
    return HttpResponse(table)



@csrf_exempt
def load_product_table(request):
    category_id = request.GET.get("id")
    print(category_id)
    products = Product.objects.filter(category=category_id)
    table = ""
    for products in products:
        print(products.id)
        table = table +  "<tr>"
        table = table + "<td><input class='checkbox' type='checkbox'></td>"
        table = table + "<td>" + str(products.sku) +"</td>"
        table = table + "<td>" + str(products.title) + "</td>"
        table = table + "<td>" + str(products.price) +"</td>"
        table = table + "<td>" + str(products.category) +"</td>"
        table = table + "<td>"
        table = table + "<a href='/ez_admin/single_product/?id="+ str(products.id) + "'>"
        table = table + "<button type='button' class='btn btn-block btn-primary btn-flat'>Edit</button>"
        table = table + "</a>"
        table = table + "</td>"
        table = table + "</tr>"
    print(table)
    return HttpResponse(table)

@csrf_exempt
def load_sc_1(request):
    category_id = request.GET.get("id")
    categories = Category.objects.filter(father_category=category_id)
    html = "<option value='0'> Select </option>"
    for sc_1 in categories:
        html = html + "<option value="+ str(sc_1.id) +"> "+ str(sc_1.title) +" </option>"
    result = {}
    
    return HttpResponse(html)

@csrf_exempt
def load_sc_2(request):
    category_id = request.GET.get("id")
    categories = Category.objects.filter(father_category=category_id)
    html = "<option value='0'> Select </option>"
    for sc_1 in categories:
        html = html + "<option value="+ str(sc_1.id) +"> "+ str(sc_1.title) +" </option>"

    return HttpResponse(html)

@csrf_exempt
def load_sc_3(request):
    category_id = request.GET.get("id")
    categories = Category.objects.filter(father_category=category_id)
    html = "<option value='0'> Select </option>"
    for sc_1 in categories:
        html = html + "<option value="+ str(sc_1.id) +"> "+ str(sc_1.title) +" </option>"

    return HttpResponse(html)

@csrf_exempt
def load_sc_4(request):
    category_id = request.GET.get("id")
    categories = Category.objects.filter(father_category=category_id)
    html = "<option value='0'> Select </option>"
    for sc_1 in categories:
        html = html + "<option value="+ str(sc_1.id) +"> "+ str(sc_1.title) +" </option>"

    return HttpResponse(html)



@csrf_exempt
def delete_featured_product(request):
    product_id = request.GET.get("id")
    product = Product.objects.get(id=product_id)
    product.home = False
    product.save()
    return HttpResponse("1")

@csrf_exempt
def scrape(request):
    url_to_scrape = request.GET.get("url")
    id = request.GET.get("id")
    payload = {
        "PIN": "256661", 
        "Password": "Rafael120588168"
    }
    session_requests = requests.session()
    login_url = "https://myeddie.edwardsutcfs.com/Logon/Logon"
    result = session_requests.post(
        login_url, 
        data = payload, 
        headers = dict(referer=login_url)
    )
    url = url_to_scrape
    result = session_requests.get(
        url, 
        headers = dict(referer = url)
    )
    tree = html.fromstring(result.content)
    bucket_elems = tree.findall(".//div[@class='column-entry']/ul/li")
    bucket_elems_2 = tree.findall(".//li[@class='featureValue']")
    
    for bucket in bucket_elems_2:
        print(bucket.text.strip())
    

    product = Product.objects.get(id=id)
    product.last_update = datetime.datetime.today()
    product.save()

    response_data = {}
    response_data['recommended'] = bucket_elems[0].text.strip()
    response_data['price'] = bucket_elems[1].text.strip()
    response_data['stock'] = bucket_elems[2].text.strip()
    response_data['weight'] = bucket_elems_2[0].text.strip()
    response_data['length'] = bucket_elems_2[1].text.strip()
    response_data['width'] = bucket_elems_2[2].text.strip()
    response_data['lastupdate'] = str(product.last_update.strftime(' %b %d, %Y'))


    return HttpResponse(json.dumps(response_data), content_type="application/json")

@csrf_exempt
def add_attribute(request):
    name = request.GET.get("name")
    description = request.GET.get("description")

    attribute = Attribute()
    attribute.name = name
    attribute.description = description
    attribute.save()
    html = '<li class="item"> <div class="product-info" style=" margin-left: 0px;"> <p class="product-title">'+ str(name) +'</p><span class="product-description">' + str(description) + '</span> </div></li>'
    return HttpResponse(html)

@csrf_exempt
def add_productattribute(request):
    attr = request.GET.get("attribute")
    attribute = Attribute.objects.get(id=attr)
    pct = request.GET.get("id")
    product = Product.objects.get(id=pct)
    description = request.GET.get("description")
    alteration = request.GET.get("alteration")
    value = request.GET.get("value")
    name = request.GET.get("name")

    attributep = AttributeProduct()
    attributep.product = product
    attributep.attribute = attribute
    attributep.description = description
    attributep.alteration = alteration
    attributep.value = value
    attributep.sku = name
    attributep.save()

    
    #html = '<li class="item"> <div class="product-info" style=" margin-left: 0px;"> <p class="product-title">'+ name +'</p><span class="product-description">' + description + '</span> </div></li>'
    html = '<li class="item" id="p'+ str(attributep.id) +'"> <div class="product-info" style=" margin-left: 0px;"> <p class="product-title"><span class="sku-lbl"></span>-'+ str(name) +'</p></p><span class="product-description"> <span class="label label-danger pull-right">X</span> <span data-toggle="modal" data-target="#addProductAttribute" class="label label-success pull-right" onclick="edit_pattribute("'+ str(value) + '","' + str(alteration) +'","' + str(description) +',"'+ str(attributep.id) +'","' + str(name) + '")" style="margin-right: 5px; cursor: pointer"> <i class="fa fa-pencil" aria-hidden="true"></i> </span> <p class="product-title" style="color: #001f3f;"> Attribute: <span style="font-weight: 300 !important">'+ str(name) +'</span> <br>Value: <span style="font-weight: 300 !important">'+ str(value) +'</span><br>Alteration: $<span style="font-weight: 300 !important">'+ str(alteration) +'</span> <span class="" style="cursor: pointer" onclick="delete_pattribute("'+ str(attributep.id) +'")"></span><br>Description: <span style="font-weight: 300 !important">'+ str(description) +'</span> </span> </div></li>'
    return HttpResponse(html)
    

@csrf_exempt
def remove_attribute(request):
    id = request.GET.get("id")
    attr = Attribute.objects.get(id=id)
    
    attr.archived = True
    attr.save()
    return HttpResponse("1")

@csrf_exempt
def remove_pattribute(request):
    id = request.GET.get("id")
    attr = AttributeProduct.objects.get(id=id)
    attr.delete()
    return HttpResponse("1")

def publish_all_product(request):
    try:
        product = Product.objects.all().update(state=1)
        return HttpResponse("1")
    except:
        return HttpResponse("0")
def publish_selected_product(request):
    try:
        id = request.GET.get("id")
        product = Product.objects.get(id=id)
        product.state = 1
        product.save()
        return HttpResponse("1")
    except:
        return HttpResponse("0")

@csrf_exempt
def update_last_update(request):
    products = Product.objects.all()
    for product in products:
        product.last_update = datetime.datetime.now().date()
        product.save()
        
    print("update last time")
    return HttpResponse("1")

@csrf_exempt
def scrape_nationwide(request):
    # Browser
    br = mechanize.Browser()

    # Cookie Jar
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)

    # Browser options
    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
    br.addheaders = [('User-agent', 'Chrome')]
    br.open('https://adiglobal.us/Pages/WebRegistration.aspx')
    # View available forms
    #for f in br.forms():
    #    print f

    # Select the second (index one) form (the first form is a search query box)
    br.select_form(name='aspnetForm')

    # User credentials
    br.form['ctl00$PlaceHolderMain$ctl00$ctlLoginView$MainLoginView$MainLogin$UserName'] = 'MATO0610'
    br.form['ctl00$PlaceHolderMain$ctl00$ctlLoginView$MainLoginView$MainLogin$Password'] = 'Rafael120588168'

    #br.links(url_regex='javascript:WebForm_DoPostBackWithOptions').click()
    # Login
    #br.form.click()
    #print(br.response().read())
    req = br.links(url_regex='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$PlaceHolderMain$ctl00$ctlLoginView$MainLoginView$MainLogin$LoginButton", "", true, "MainLoginValidation_SignIn", "", false, true))')
    print(req)
    br.open(req)
    print br.response().read()
    
    #print(br.open('https://adiglobal.us/Pages/Product.aspx?pid=FL-SD355&Category=0000').read())


    return HttpResponse("1")