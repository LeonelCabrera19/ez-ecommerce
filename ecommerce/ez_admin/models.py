from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Manufacturer(models.Model):
    #archived = models.BooleanField()
    title = models.CharField(max_length=800)
    url = models.CharField(max_length=800, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    
    def __str__(self):
        return u'{0}'.format(self.title)
    
class Details(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=14,null=True, blank=True, default='')
    work_phone = models.CharField(max_length=14,null=True, blank=True, default='')
    organization = models.CharField(max_length=44,null=True, blank=True, default='')
    title = models.CharField(max_length=44,null=True, blank=True, default='')
    department = models.CharField(max_length=44,null=True, blank=True, default='')
    address = models.CharField(max_length=800,null=True, blank=True, default='')
    address2 = models.CharField(max_length=800,null=True, blank=True, default='')
    city = models.CharField(max_length=800,null=True, blank=True, default='')
    state = models.CharField(max_length=200,null=True, blank=True, default='')
    zipcode = models.CharField(max_length=5,null=True, blank=True, default='')
    country = models.CharField(max_length=800,null=True, blank=True, default='')
    company_logo = models.CharField(max_length=800,null=True, blank=True, default='')
    user_image = models.CharField(max_length=800,null=True, blank=True, default='')
    archived = models.BooleanField(default=False)

# class Company(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     company = models.CharField(max_length=700)
#     address = models.CharField(max_length=800)
#     email = models.CharField(max_length=700)
#     phone = models.CharField(max_length=700)
class ConfigSchema(models.Model):
    qty = models.IntegerField()
    cat_no = models.CharField(max_length=700, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    weight = models.CharField(max_length=20, null=True, blank=True)
    length = models.CharField(max_length=20, null=True, blank=True)
    width = models.CharField(max_length=20, null=True, blank=True)
    depth = models.CharField(max_length=20, null=True, blank=True)
    factor = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    purchase = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    cat_sheet = models.CharField(max_length=20, null =True, blank=True)
    group = models.CharField(max_length=20,null=True, blank=True)
    category = models.CharField(max_length=20,null=True, blank=True)
    function = models.CharField(max_length=20,null=True, blank=True)
    addressable = models.CharField(max_length=20,null=True, blank=True)
    fixture = models.CharField(max_length=20,null=True, blank=True)
    construction = models.CharField(max_length=20,null=True, blank=True)
    subcategory = models.CharField(max_length=20,null=True, blank=True)
    title = models.CharField(max_length=20,null=True, blank=True)