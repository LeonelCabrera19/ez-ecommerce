from django.contrib import admin
from shop.models import *
# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ('title','sku','category', 'price', 'stock')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'father_category')

class AttributeAdmin(admin.ModelAdmin):
    list_display = ('name',)

class AttributeProductAdmin(admin.ModelAdmin):
    list_display = ('attribute','value','alteration')

admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Attribute, AttributeAdmin)
admin.site.register(AttributeProduct, AttributeProductAdmin)