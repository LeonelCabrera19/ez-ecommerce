from django.db import models
from django.template.defaultfilters import slugify
from ez_admin.models import *

class Category(models.Model):
    title = models.CharField(max_length=800)
    father_category = models.ForeignKey('self', null=True, blank=True)

    def __str__(self):
        return u'{0}'.format(self.title)
    
    def child_categories(self):
        return Category.objects.filter(father_category=self)

# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=800, null=True, blank=True)
    slug = models.SlugField(max_length=255, null=True, unique=True)
    short_description = models.TextField(null=True, blank=True)
    long_description = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    featured = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    sku = models.CharField(max_length=60)
    mfr = models.ForeignKey(Manufacturer, default=1)
    mfr_purchase = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    length = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    width = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    height = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    weight = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    stock = models.IntegerField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    home = models.BooleanField(default=False)
    mfr_url = models.CharField(max_length=800, null=True, blank=True)
    part_number = models.CharField(max_length=600,null=True, blank=True)
    factor = models.DecimalField(max_digits=6, decimal_places=2, default=2)
    last_update = models.DateField(null=True, default="2017-03-08")
    nation_stock = models.CharField(max_length=45,null=True, blank=True)
    state = models.IntegerField(max_length=6,default=2)
    purchase_price_whs = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title) + "-" + slugify(self.sku)
        super(Product, self).save(*args, **kwargs)

    def update(self, *args, **kwargs):
        self.slug = slugify(self.title)
        print("updte slug")
        super(Product, self).save(*args, **kwargs)

    def __str__(self):
        return u'{0}'.format(self.sku)
    

class ProductRelated(models.Model):
    product_father = models.ForeignKey(Product, related_name='product_father')
    product_related = models.ForeignKey(Product, related_name='product_related')

class Attribute(models.Model):
    name = models.CharField(max_length=400)
    description = models.TextField(null=True, blank=True)
    archived = models.BooleanField(default=False)
    def __str__(self):
        return u'{0}'.format(self.name)
    

class AttributeProduct(models.Model):
    attribute = models.ForeignKey(Attribute)
    product = models.ForeignKey(Product)
    value = models.CharField(max_length=600)
    image_path = models.CharField(max_length=1000,null=True)
    description = models.TextField(null=True)
    alteration = models.DecimalField(max_digits=6, decimal_places=2,null=True)
    sku = models.CharField(max_length=400)
    def __str__(self):
        return u'{0}'.format(self.value)
