from django.shortcuts import render
from shop.models import *
# Create your views here.
def single_product(request, slug):
    product = Product.objects.get(slug=slug)
    attributes = Attribute.objects.all()
    pattributes = AttributeProduct.objects.filter(product=product)
    just_attributes = AttributeProduct.objects.values_list('attribute', flat=True).filter(product=product)
    for attribute in attributes:
        if attribute.id in just_attributes:
            print("True")
            
            
    context= {
        'product': product,
        'attributes':attributes,
        'pattributes':pattributes,
        'just_attributes':just_attributes
    }

    return render(request, 'single_product.html',context)

def home_shop(request):
    featured_products = Product.objects.filter(home=True)
    context = {
        'featured_products': featured_products
    }
    return render(request,'shop_home.html', context)

def checkout_product(request):
    context = {
        'title':'test'
    }
    return render(request,'checkout_product.html', context)