# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0008_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(default=4, to='shop.Category'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='category',
            name='father_category',
            field=models.ForeignKey(blank=True, to='shop.Category', null=True),
        ),
    ]
