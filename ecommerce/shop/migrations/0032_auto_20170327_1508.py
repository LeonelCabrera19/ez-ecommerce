# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0031_auto_20170321_0925'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='purchase_price_whs',
            field=models.DecimalField(null=True, max_digits=6, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='state',
            field=models.IntegerField(default=2, max_length=6),
        ),
    ]
