# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0009_auto_20170209_1434'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='part_number',
            new_name='sku',
        ),
    ]
