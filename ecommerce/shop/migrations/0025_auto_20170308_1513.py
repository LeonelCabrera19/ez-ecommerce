# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0024_auto_20170308_1505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='nation_stock',
            field=models.CharField(default=1, max_length=45),
            preserve_default=False,
        ),
    ]
