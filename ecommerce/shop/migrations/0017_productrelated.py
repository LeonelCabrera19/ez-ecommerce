# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0016_auto_20170222_1620'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductRelated',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_father', models.ForeignKey(related_name='product_father', to='shop.Product')),
                ('product_related', models.ForeignKey(related_name='product_related', to='shop.Product')),
            ],
        ),
    ]
