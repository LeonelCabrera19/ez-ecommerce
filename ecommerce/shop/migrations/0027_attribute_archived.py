# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0026_attribute'),
    ]

    operations = [
        migrations.AddField(
            model_name='attribute',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
