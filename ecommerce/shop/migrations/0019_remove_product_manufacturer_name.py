# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0018_auto_20170223_1009'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='manufacturer_name',
        ),
    ]
