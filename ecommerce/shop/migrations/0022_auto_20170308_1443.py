# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0021_auto_20170306_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='mfr_url',
            field=models.CharField(max_length=800, null=True, blank=True),
        ),
    ]
