# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0027_attribute_archived'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=600)),
                ('image_path', models.CharField(max_length=1000)),
                ('description', models.TextField(null=True)),
                ('alteration', models.DecimalField(max_digits=6, decimal_places=2)),
                ('attribute', models.ForeignKey(to='shop.Attribute')),
                ('product', models.ForeignKey(to='shop.Product')),
            ],
        ),
    ]
