# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0030_attributeproduct_sku'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='state',
            field=models.IntegerField(default=1, max_length=6),
        ),
        migrations.AlterField(
            model_name='product',
            name='factor',
            field=models.DecimalField(default=2, max_digits=6, decimal_places=2),
        ),
    ]
