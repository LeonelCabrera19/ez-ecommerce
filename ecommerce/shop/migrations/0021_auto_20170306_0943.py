# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_product_mfr_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='part_number',
            field=models.CharField(default=' ', max_length=600),
            preserve_default=False,
        ),
    ]
