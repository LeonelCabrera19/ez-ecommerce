# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0034_auto_20170404_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='nation_stock',
            field=models.CharField(max_length=45, null=True, blank=True),
        ),
    ]
