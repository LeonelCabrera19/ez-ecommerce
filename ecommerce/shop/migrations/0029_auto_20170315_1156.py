# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0028_attributeproduct'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attributeproduct',
            name='alteration',
            field=models.DecimalField(null=True, max_digits=6, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='attributeproduct',
            name='image_path',
            field=models.CharField(max_length=1000, null=True),
        ),
    ]
