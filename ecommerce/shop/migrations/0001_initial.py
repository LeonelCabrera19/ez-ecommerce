# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=400)),
                ('short_description', models.CharField(max_length=600)),
                ('long_description', models.CharField(max_length=3000)),
                ('price', models.DecimalField(max_digits=6, decimal_places=2)),
            ],
        ),
    ]
