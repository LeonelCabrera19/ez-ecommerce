# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0029_auto_20170315_1156'),
    ]

    operations = [
        migrations.AddField(
            model_name='attributeproduct',
            name='sku',
            field=models.CharField(default=1, max_length=400),
            preserve_default=False,
        ),
    ]
