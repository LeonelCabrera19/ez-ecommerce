# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0035_auto_20170404_0944'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 6, 15, 3, 38, 950177, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
