# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0038_remove_product_mfr_cost'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='mfr_cost',
        ),
    ]
