# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20170210_1149'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='home',
            field=models.BooleanField(default=False),
        ),
    ]
