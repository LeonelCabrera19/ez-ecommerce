# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0019_remove_product_manufacturer_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='mfr_url',
            field=models.CharField(default='', max_length=800),
            preserve_default=False,
        ),
    ]
