# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20170208_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='featured',
            field=models.TextField(default='test'),
            preserve_default=False,
        ),
    ]
