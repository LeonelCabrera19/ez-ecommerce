# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0023_auto_20170308_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='factor',
            field=models.DecimalField(default=1, max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='nation_stock',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='part_number',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
    ]
