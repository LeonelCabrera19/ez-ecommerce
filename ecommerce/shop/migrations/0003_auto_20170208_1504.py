# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20170208_1100'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='slug',
            field=models.CharField(default='test', max_length=800),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='short_description',
            field=models.CharField(max_length=800),
        ),
        migrations.AlterField(
            model_name='product',
            name='title',
            field=models.CharField(max_length=800),
        ),
    ]
