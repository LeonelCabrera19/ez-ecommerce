# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_admin', '0003_auto_20170222_1626'),
        ('shop', '0017_productrelated'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='mfr',
            field=models.ForeignKey(default=1, to='ez_admin.Manufacturer'),
        ),
        migrations.AddField(
            model_name='product',
            name='mfr_cost',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='mfr_purchase',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True),
        ),
    ]
