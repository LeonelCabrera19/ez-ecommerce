# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_product_featured'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='height',
            field=models.DecimalField(default='1', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='length',
            field=models.DecimalField(default='1', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='manufacturer_name',
            field=models.CharField(default='1', max_length=300),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='part_number',
            field=models.CharField(default='1', max_length=60),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='weight',
            field=models.DecimalField(default='1', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='width',
            field=models.DecimalField(default='1', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
    ]
