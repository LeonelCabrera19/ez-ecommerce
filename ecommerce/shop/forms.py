import re
from django import forms
from .models import *

class ProductForm(forms.ModelForm):
    title = forms.CharField(required=True)

    class Meta:
        model = Product
        exclude = ('slug','last_update','state',)
    
class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        exclude = ()

class AttributeProductForm(forms.ModelForm):
    class Meta:
        model = AttributeProduct
        exclude = ()
        